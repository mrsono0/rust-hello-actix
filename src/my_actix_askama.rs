use actix_web::{web, HttpRequest, HttpResponse, Result};
use askama::Template;
use std::collections::HashMap;

#[derive(Template)]
#[template(path = "user.html")]
struct UserTemplate<'a> {
  name: &'a str,
  text: &'a str,
}

#[derive(Template)]
#[template(path = "index.html")]
struct Index;

pub async fn index(req: HttpRequest, query: web::Query<HashMap<String, String>>) -> Result<HttpResponse> {
  let name1 = req.match_info().get("name1").unwrap_or("Hi~~~");
  println!("{}", name1);
  let s = if let Some(name) = query.get("name") {
    UserTemplate {
      name,
      text: "Welcome!",
    }
    .render()
    .unwrap()
  } else {
    Index.render().unwrap()
  };
  Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
