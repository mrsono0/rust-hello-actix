use std::env;
use actix_web::{web, App, HttpServer};
mod my_actix_web;
mod my_actix_askama;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
  let args: Vec<String> = env::args().collect();
  println!("{:?}", args);
  if args.len() > 1 && args[1].eq("1") {
    println!("my_actix_web");
    HttpServer::new(|| {
      App::new()
        .route("/", web::get().to(my_actix_web::greet))
        .route("/{name}", web::get().to(my_actix_web::greet))
        .route("/hi/", web::get().to(my_actix_web::hi))
        .route("/hi/{name}", web::get().to(my_actix_web::hi))
    })
    .bind("127.0.0.1:8000")?
    .run()
    .await
  } else {
    println!("my_actix_askama");
    HttpServer::new(move || {
      App::new()
      .service(web::resource("/").route(web::get().to(my_actix_askama::index)))
      .service(web::resource("/{name1}").route(web::get().to(my_actix_askama::index)))

    })
    .bind("127.0.0.1:8000")?
    .run()
    .await
  }
}
