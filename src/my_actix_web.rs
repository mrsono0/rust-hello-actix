use actix_web::{HttpRequest, Responder};

pub async fn greet(req: HttpRequest) -> impl Responder {
    let name = req.match_info().get("name").unwrap_or("World");
    format!("Hello {}!", &name)
}
pub async fn hi(req: HttpRequest) -> impl Responder {
    let name = req.match_info().get("name").unwrap_or("Hi~~~");
    format!("Hello {}~~~", &name)
}
