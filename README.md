# Install Rust

## Using rustup (Recommended)

It looks like you’re running macOS, Linux, or another Unix-like OS. To download Rustup and install Rust, run the following in your terminal, then follow the on-screen instructions.

```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh  
```

# Rust Language Server (RLS)

## Step 1: Install rustup

You can install [rustup](http://rustup.rs/) on many platforms. This will help us quickly install the
RLS and its dependencies.

If you already have rustup installed, update to ensure you have the latest
rustup and compiler:

```
rustup update
```

If you're going to use the VSCode extension, you can skip step 2.

## Step 2: Install the RLS

Once you have rustup installed, run the following commands:

```
rustup component add rls rust-analysis rust-src
```

# Rust (rls) Quick start

## Install rustup (Rust toolchain manager).

Install this extension from the VSCode Marketplace (or by entering ext install rust-lang.rust at the command palette Ctrl+P).  
(Skip this step if you already have Rust projects that you'd like to work on.) Create a new Rust project by following these instructions.  
Open a Rust project (File > Add Folder to Workspace...). Open the folder for the whole project (i.e., the folder containing 'Cargo.toml'),  
not the 'src' folder.  
You'll be prompted to install the RLS. Once installed, the RLS should start building your project.  

## Configuration

This extension provides options in VSCode's configuration settings. These include rust.*, which are passed directly to RLS, and the rust-client.* ,  which mostly deal with how to spawn it or debug it. You can find the settings under File > Preferences > Settings; they all have Intellisense help.  

Some highlights:  
rust.show_warnings - set to false to silence warnings in the editor.  
rust.all_targets - build and index code for all targets (i.e., integration tests, examples, and benches)  
rust.cfg_test - build and index test code (i.e., code with #[cfg(test)]/#[test])  
rust-client.channel - specifies from which toolchain the RLS should be spawned  

# Rust ML, DL libraries.
[State of Machine Learning in Rust](https://ehsanmkermani.com/2019/05/13/state-of-machine-learning-in-rust/)  
[rust-ml/discussion](https://github.com/rust-ml/discussion)  
[Machine Learning library for Rust](https://github.com/AtheMathmo/rusty-machine)  
[rust numpy ndarray](https://github.com/rust-ndarray/ndarray)  
[rust pandas black-jack](https://docs.rs/crate/black-jack/0.1.0) 
[Rust bindings for tensorflow](https://lib.rs/install/tensorflow)  
[Rust bindings for tensorflow github](https://github.com/tensorflow/rust)  
[Rust bindings for PyTorch](https://github.com/LaurentMazare/tch-rs)  
[ml libraries list](https://lib.rs/science/ml),
[ml libraries list](https://libraries.io/search?keywords=machine-learning&languages=Rust)  


# Rust Web app libraries.
[web-programming](https://crates.io/categories/web-programming)  
[maud jinja2](https://maud.lambda.xyz/web-frameworks.html)  
[askama jinja2](https://github.com/djc/askama)  
